/**
 * Created by EYAD on 2017-12-12.
 */
app.controller("search", ($scope, $timeout, $http, CONSTANTS)=> {

    // On Click over any image this function will assign that image to object so the overlay will show up.
    $scope.show_image_overlay = image => {
        console.log(image);
        $scope.show_image = image;
    };

    // Build Image url using given image data.
    $scope.build_url = (image, size='q') => {
        if(!image) return;

        return "https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+"_"+size+".jpg";
    };


    $scope.search_keyword = "";


    // Method to search by filckr API, by default it will search form page number 1 if no page given.
    $scope.start_search = (page= 1) => {

        $scope.start_searching = true;
        $scope.result = null;


        // Check if the Flickr API key is exists or not.
        if(!CONSTANTS.FLICKR_KEY){
            console.warn("Please open 'src/app/constants.js' file and add you flickr api key to be able start searching.");
            alert("You must add Flickr API Key, before start searching. for more details see console log");
            return;
        }

        // Start calling API with sending the search key.
        $http(
            {
                url: 'https://api.flickr.com/services/rest/',
                method: 'GET',
                params: {
                    method: 'flickr.photos.search',
                    tags: $scope.search_keyword, //'soccer',
                    api_key: CONSTANTS.FLICKR_KEY,
                    format: 'json',
                    per_page: 10,
                    page: page
                }
            }
        ).then(
            res => {
                $scope.start_searching = false;
                $scope.searched_keyword = $scope.search_keyword;
                // Flickr returns JSONP response like // jsonFlickrApi({})
                // So pass that response to get plain json inside the function.
                $scope.result = eval("("+res.data + ")");
            }
        ).catch(
            res=> {
                // Cach the error if exists.
                $scope.start_searching = false;
                $scope.searched_keyword = $scope.search_keyword;
                console.log(res);
            }
        );


        // Function to get the json from inside JSONP function.
        let jsonFlickrApi = response => {
            return response;
        }
    };

});