
Please before run project install the project dependencies using command below.

You have own **bower** installed in your system.

```bower install```

After that open the "```src/app/constants.js```" path and insert your **Flickr API key**.

That is all.
### Example of working test.
------------------------

![picture](example/example.PNG)


Thanks.

Eyad Farra

FullStack Web Developer